
# Collide Bootstrap - Responsive Business Hugo Theme Built on Bootstrap

[Theme Demo](https://collide-public.gitlab.io/hugo-collide-bootstrap)

# Hugo Tips & Tricks
It is important to note that all _index.md content files will render according to a list template and not according to a single page template.

# Favicons
Use favicon generator to generate favicon .zip file. Extract to static/favicons directory.

# Custom CSS
Override existing CSS or add new things in assets/css

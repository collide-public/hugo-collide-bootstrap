$(document).ready(function() {
    $(window).scroll(function() {
    // if scrolled past 20 ptx from the top, add background color to navbar, change text to black
    if ($(this).scrollTop() > 20) {
        $("#navigation").addClass("bg-white");
        $("#nav-button").removeClass("custom-toggler-white")
        $("#nav-button").addClass("custom-toggler-black")
        $(".nav-link").removeClass("text-white")
        $(".nav-link").addClass("text-dark")


    } else {
        // if navbar is hidden, remove the background color and change text color to white
        if ($("#navbar").is(":hidden")) {
            $("#navigation").removeClass("bg-white");
            $("#nav-button").removeClass("custom-toggler-black")
            $("#nav-button").addClass("custom-toggler-white")
            $(".nav-link").removeClass("text-dark")
            $(".nav-link").addClass("text-white")
        }
    }
    });
    // when nav-button is clicked, add background color, and change content color
    $( "#nav-button" ).click(function() {
        $( "#navigation" ).addClass( "bg-white" );
        $("#nav-button").removeClass("custom-toggler-white")
        $("#nav-button").addClass("custom-toggler-black")
        $(".nav-link").removeClass("text-white")
        $(".nav-link").addClass("text-dark")
    });
});

function handler(event) {
    var request = event.request;
    var uri = request.uri;

    // Check whether the URI is missing a file name.
    if (uri.endsWith('/')) {
        request.uri += 'index.html';
    }
    // Check whether the URI is missing a file extension.
    else if (!uri.includes('.')) {
        request.uri += '/index.html';
    }

    return request;
}
